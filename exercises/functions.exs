list_concat = fn a, b -> a ++ b end
IO.puts inspect(list_concat.([:a, :b], [:c, :d]))

sum = fn a, b, c -> a + b + c end
IO.puts inspect(sum.(1,2,3))

pair_tuple_to_list = fn {a, b} -> [a, b] end
IO.puts inspect pair_tuple_to_list.({ 1234, 5678 })

fizzbuzz = fn
  0, 0, _ -> "FizzBuzz"
  0, _, _ -> "Fizz"
  _, 0, _ -> "Buzz"
  _, _, a -> a
end

IO.puts fizzbuzz.(0,0,:a)
IO.puts fizzbuzz.(0,1,:a )
IO.puts fizzbuzz.(1,0,:a)
IO.puts " --- "

fizzbuzz_rem = fn n ->
  fizzbuzz.(rem(n,3), rem(n,5), n)
end

IO.puts fizzbuzz_rem.(10)
IO.puts fizzbuzz_rem.(11)
IO.puts fizzbuzz_rem.(12)
IO.puts fizzbuzz_rem.(13)
IO.puts fizzbuzz_rem.(14)
IO.puts fizzbuzz_rem.(15)
IO.puts fizzbuzz_rem.(16)
IO.puts fizzbuzz_rem.(17)


